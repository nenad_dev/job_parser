<?php
      // start session   
      session_start(); 
      
      // clear/destroy session
      session_destroy();
      
      // redirect to login page
      header('Location: index.php');

