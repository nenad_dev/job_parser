<?php

// include script configuration
require realpath(dirname(__FILE__)) . "/configuration.php";

// include script functions
require realpath(dirname(__FILE__)) . "/functions.php";

// connect to db
$con = db_connect($db_host, $db_name, $db_user, $db_password);

// delete items inserted before expiration date
$expiration_date = date('Y-m-d H:i:s', strtotime('-1 month'));
$query = "DELETE FROM `jobs` WHERE `insert_date` < '$expiration_date'";
do_query($con, $query);
