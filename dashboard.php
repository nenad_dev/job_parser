<?php
// start session   
session_start();

// check is use logged in
if ($_SESSION['is_logged_in'] != "yes") {
    header('Location: index.php');
}

// security constant for including files
define("SECURE_ACCESS_INCLUDE_FILE", "ALLOW ACCESS");

// include script configuration
require_once "configuration.php";

// include script functions
require_once "functions.php";

// connect to db
$con = db_connect($db_host, $db_name, $db_user, $db_password);

// get items
$items = get_items();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/jquery-ui.css"  type="text/css" rel="stylesheet"  />
        <link href="css/bootstrap.min.css" type="text/css" rel="stylesheet" />
        <link href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css" type="text/css" rel="stylesheet" />
        <link href="css/style.css" type="text/css" rel="stylesheet" />
        <script src="js/jquery-1.12.4.js" type="text/javascript" language="javascript" ></script>
        <script src="js/jquery.dataTables.min.js" type="text/javascript" language="javascript" ></script>
        <script src="js/dataTables.bootstrap.min.js" type="text/javascript" language="javascript" ></script>
        <script src="js/jquery.validate.js" type="text/javascript" language="javascript" ></script>
        <script src="js/jquery-ui.min.js" type="text/javascript" language="javascript" ></script>
        <script src="js/script.js" type="text/javascript" ></script>
    </head>
    <body>
        <div id="wrapper">
            <div id="right_menu">
                <a href="logout.php">Logout</a>
            </div>
            <div style="clear: both;"></div>
            <div id="apps_table_cont" >
                <div id="action_btns" >
                    <button id="new_btn">Add New</button>
                    <button id="edit_btn">Edit</button>
                    <button id="delete_btn">Delete</button>
                </div>
                <table id="apps_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Company</th>
                            <th>Job Location</th>
                            <th>Source</th>
                            <th>Date Inserted</th>
                            <th>Featured</th>
                            <th>Job Url</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Title</th>
                            <th>Company</th>
                            <th>Job Location</th>
                            <th>Source</th>
                            <th>Date Inserted</th>
                            <th>Featured</th>
                            <th>Job Url</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        <?php
                        // loop through apps
                        foreach ($items as $item) {
                            ?>
                            <tr data-id='<?php echo $item['id']; ?>' >
                                <td class="title"><?php echo $item['title']; ?></td>
                                <td class="description"><?php echo $item['description']; ?></td>
                                <td class="location"><?php echo $item['location']; ?></td>
                                <td class="source"><?php echo $item['source']; ?></td>
                                <td class="insert_date"><?php echo $item['insert_date']; ?></td>
                                <td class="featured"><?php echo $item['featured']; ?></td>
                                <td class="link"><a href="<?php echo $item['link']; ?>" ><?php echo $item['link']; ?></a></td>
                            </tr>
                        <?php } ?>   
                    </tbody>
                </table>
                <p>Developed by: <a href="https://fiverr.com/nenads">https://fiverr.com/nenads</a></p>
                <div id="add_update_popup" style="display:none" >
                    <form method="post" action="" id="add_update_form" name="add_update_form">
                        <div id="main_form" >
                            <div>
                                <label for="title">Title</label>
                                <input type="text" name="title" value="" id="title" required />
                            </div>
                            <div style="clear: both;"></div>
                            <div>
                                <label for="description">Description</label>
                                <input type="text" name="description" value="" id="description" required />
                            </div> 
                            <div style="clear: both;"></div>
                            <div>
                                <label for="location">Job Location</label>
                                <input type="text" name="location" value="" id="location" required />
                            </div> 
                            <div style="clear: both;"></div>
                            <div>
                                <label for="source">Source</label>
                                <input type="text" name="source" value="" id="source" required />
                            </div> 
                            <div style="clear: both;"></div>
                            <div>
                                <label for="featured">Featured</label>
                                <select name="featured" id="featured" >
                                    <option value="1">Featured 1</option>
                                    <option value="2">Featured 2</option>
                                    <option value="0">No Featured</option>
                                </select>    
                            </div> 
                            <div style="clear: both;"></div>
                            <div>
                                <label for="link">Link</label>
                                <input type="url" name="link" value="" id="link" required />
                            </div> 
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
