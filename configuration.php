<?php

// protect file for direct access
if (!defined('SECURE_ACCESS_INCLUDE_FILE') && SECURE_ACCESS_INCLUDE_FILE != "ALLOW ACCESS") {
    exit();
}

// database config
$db_host = 'localhost';
$db_name = 'job_parser';
$db_user = 'root';
$db_password = 'test';

// admin login info
$admin_username = "admin";
$admin_password = "pass";

// enable mysql error logging in script - TRUE for enabled and FALSE for disabled
$enable_mysql_error_log = TRUE;

// set display errors status
ini_set('display_errors', 1); // 1-turn on all error reporings 0-turn off all error reporings
error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);

// change max execution time to unlimitied
ini_set('max_execution_time', 0);

