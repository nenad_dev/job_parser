<?php

// security constant for including files
define("SECURE_ACCESS_INCLUDE_FILE", "ALLOW ACCESS");

// include script configuration
require realpath(dirname(__FILE__)) . "/configuration.php";

// include script functions
require realpath(dirname(__FILE__)) . "/functions.php";

// connect to db
$con = db_connect($db_host, $db_name, $db_user, $db_password);

header("Content-Type: application/rss+xml; charset=ISO-8859-1");

session_start();
ob_start();

$rssfeed = '<?xml version="1.0" encoding="ISO-8859-1"?>';
$rssfeed .= '<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">';
$rssfeed .= '<channel>';
$rssfeed .= '<title>My RSS feed Visible Text</title>';
$rssfeed .= '<link></link>';
$rssfeed .= '<description>This is an example RSS feed Visible text</description>';
$rssfeed .= '<language>en-us</language>';
$rssfeed .= "<pubDate></pubDate>";
$rssfeed .= "<copyright>Copyright (C) </copyright>";
$rssfeed .= '<atom:link href="" rel="self" type="application/rss+xml" />';
$rssfeed .= '<image><url>http://themixnow.fm/wp-content/uploads/2012/03/RSS.gif</url>
    <title>My RSS feed Visible Text</title><width>80</width> <height>80</height>
    <link></link></image>';

// get parameters
if (isset($_GET["q"])) {
    $q = urlencode(filter_var($_GET["q"], FILTER_SANITIZE_STRING));
} else {
    $q = "";
}

if (isset($_GET["location"])) {
    $location = urlencode(filter_var($_GET["location"], FILTER_SANITIZE_STRING));
} else {
    $location = "";
}

if (isset($_GET["source"])) {
    $source = urlencode(filter_var($_GET["source"], FILTER_SANITIZE_STRING));
} else {
    $source = "";
}

if (isset($_GET["per_page"])) {
    $per_page = (int) filter_var($_GET["per_page"], FILTER_SANITIZE_STRING);
} else {
    $per_page = 20;
}

if (isset($_GET["exclude"])) {
    $exclude = filter_var($_GET["exclude"], FILTER_SANITIZE_STRING);
} else {
    $exclude = "";
}

// featured jobs
if ($exclude != "featured") {
    $query1 = "SELECT * FROM `jobs` WHERE `featured`=1 ORDER BY `insert_date` DESC";
    add_rss_content($query1);
    if ($q != "") {
        $query2 = "SELECT * FROM `jobs` WHERE `title` LIKE '%$q%' AND `location` LIKE '%$location%' AND `source` LIKE '%$source%' AND `featured`=2 ORDER BY `insert_date` DESC LIMIT $per_page";
        add_rss_content($query2);
    }
}

// not featured jobs
$query3 = "SELECT * FROM `jobs` WHERE `title` LIKE '%$q%' AND `location` LIKE '%$location%' AND `source` LIKE '%$source%' AND featured=0 ORDER BY `insert_date` DESC LIMIT $per_page";
add_rss_content($query3);

$rssfeed .= "</channel>";
$rssfeed .= "</rss>";

echo $rssfeed;
