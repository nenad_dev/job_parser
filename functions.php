<?php

// protect file for direct access
if (!defined('SECURE_ACCESS_INCLUDE_FILE') && SECURE_ACCESS_INCLUDE_FILE != "ALLOW ACCESS") {
    exit();
}

function db_connect($db_host, $db_name, $db_user, $db_password) {

    $con = mysqli_connect($db_host, $db_user, $db_password, $db_name);

    if (mysqli_connect_errno()) {
        echo "Failed to connect to MySQL: " . mysqli_connect_error();
        exit;
    }

    return $con;
}

function do_query($con, $sql) {

    global $enable_mysql_error_log;

    $result = mysqli_query($con, $sql);

    if (!$result && $enable_mysql_error_log) {
        $handler = @fopen(realpath(dirname(__FILE__)) . "/mysql_errors.txt", "a");
        $log_content = "Error Date: " . date('d-m-Y H:i:s') . "\n";
        $log_content.= "MySQL Error: " . mysqli_error($con) . "\n\n";
        fwrite($handler, $log_content);
        fclose($handler);
    }

    return $result;
}

function get_items_karir($scrap_url) {

    $xml = simplexml_load_file($scrap_url);

// loop through items on current page
    foreach ($xml->url as $item) {
        if (strpos($item->loc, "/opportunities/") !== FALSE) {
            if (!item_exists($item->loc)) {
                get_single_item_karir($item->loc);
            }
        }
    }
}

function get_single_item_karir($item_url) {

    $location = "";

    $html = get_html($item_url);

    if ($html && is_object($html) && isset($html->nodes)) {

        $title_string = get_value($html, ".b-opportunity-mini-company-profile h5", 0, "text");

        $title_parts = explode("-", $title_string);
        $title = trim($title_parts[0]);

        $info_items = $html->find(".b-stat__footer");
        foreach ($info_items as $info_item) {
            $info_heading = trim($info_item->plaintext);
            $info_value_cont = $info_item->prev_sibling();
            if ($info_value_cont) {
                $info_value = trim($info_value_cont->plaintext);
                if ($info_heading == "Location") {
                    $location = $info_value;
                }
            }
        }

        $company = get_value($html, ".b-opportunity-mini-company-profile__company", 0, "text");
        $date = get_value($html, ".b-opportunity-mini-company-profile__time", 0, "text");

        if ($title) {
            insert_in_db($title, $company, $location, $date, $item_url, "karir.com", 0);
        }

        $html->clear();
    }
}

function get_items_blogger($scrap_url) {

    $xml = simplexml_load_file($scrap_url);

    foreach ($xml->entry as $entry) {

        $location = "";
        $item_url = "";

        $title = $entry->title;
        $date = $entry->published;

        $title_parts = explode("-", $title);
        $company = trim($title_parts[0]);

        foreach ($entry->link as $link) {
            $rel = $link->attributes()->rel;
            if ($rel == "alternate") {
                $item_url = $link->attributes()->href;
            }
        }

        if (!item_exists($item_url)) {
            insert_in_db($title, $company, $location, $date, $item_url, "blogger.com", 0);
        }
    }
}

function get_items_karirpad($scrap_url) {

    $xml = simplexml_load_file($scrap_url);

    if (isset($xml->jobs)) {
        foreach ($xml->jobs->job as $job) {
            $title = $job->title;
            $company = $job->employer;
            $item_url = $job->url;
            $location = $job->city;
            $date = $job->updated;

            if (!item_exists($item_url)) {
                insert_in_db($title, $company, $location, $date, $item_url, "karirpad.com", 0);
            }
        }
    }
}

function get_items_bosloker($scrap_url, $featured) {
    $html = get_html($scrap_url);

    if ($html && is_object($html) && isset($html->nodes)) {

        $items = $html->find(".jobs-listing .jobs-content");
        foreach ($items as $item) {
            $url = get_value($item, ".cs-post-title a", 0, "href");
            if (!item_exists($url)) {

                $title = get_value($item, ".cs-post-title a", 0, "text");
                $location = get_value($item, ".cs-location", 0, "text");
                $date = get_value($item, ".cs-post-date", 0, "text");

                $company = get_bosloker_company($url);

                insert_in_db($title, $company, $location, $date, $url, "id.bosloker.com", $featured);
            }
        }

        $html->clear();
    }
}

function get_bosloker_company($item_url) {

    $company = "";

    $html = get_html($item_url);

    if ($html && is_object($html) && isset($html->nodes)) {

        $company = get_value($html, ".jobs-info .cs-text strong", 0, "text");

        $html->clear();
    }

    return $company;
}

function get_items_urbanhire($scrap_url) {

    $html = get_html($scrap_url);

    if ($html && is_object($html) && isset($html->nodes)) {

        $items = $html->find("article");
        foreach ($items as $item) {

            $url = "https://www.urbanhire.com" . get_value($item, "[itemprop=url]", 0, "href");
            if (!item_exists($url)) {

                $title = get_value($item, "[itemprop=title]", 0, "text");
                $company = get_value($item, "[itemprop=hiringOrganization]", 0, "text");

                $date = "";
                $date_sibling = $item->find(".icon-clock", 0);
                if ($date_sibling) {
                    $date_cont = $date_sibling->parent();
                    if ($date_cont) {
                        $date = $date_cont->plaintext;
                    }
                }

                $location = get_urbanire_location($url);

                insert_in_db($title, $company, $location, $date, $url, "urbanhire.com", 0);
            }
        }

        $html->clear();
    }
}

function get_urbanire_location($item_url) {

    $location = "";

    $html = get_html($item_url);

    if ($html && is_object($html) && isset($html->nodes)) {

        $location_sibling = $html->find(".icon-location", 0);
        if ($location_sibling) {
            $location_cont = $location_sibling->parent();
            if ($location_cont) {
                $location = trim($location_cont->plaintext);

                if (substr($location, -1) == ",") {
                    $location = substr($location, 0, -1);
                }
            }
        }

        $html->clear();
    }

    return $location;
}

function get_value($element, $selector_string, $index, $type = "text") {
    $value = "";
    $cont = $element->find($selector_string, $index);
    if ($cont) {
        if ($type == "href") {
            $value = $cont->href;
        } elseif ($type == "src") {
            $value = $cont->src;
        } elseif ($type == "text") {
            $value = trim($cont->plaintext);
        } elseif ($type == "content") {
            $value = trim($cont->content);
        } else {
            $value = $cont->innertext;
        }
    }

    return trim($value);
}

function insert_in_db($title, $description, $location, $date, $link, $source, $featured) {

    global $con;

    $title = mysqli_real_escape_string($con, $title);
    $description = mysqli_real_escape_string($con, $description);
    $location = mysqli_real_escape_string($con, $location);
    $link = mysqli_real_escape_string($con, $link);
    $date = mysqli_real_escape_string($con, $date);
    $featured = mysqli_real_escape_string($con, $featured);
    $insert_date = date('Y-m-d H:i:s');

    $insert_sql = "INSERT INTO `jobs` (`title`,`description`,`location`,`date`,`link`,`source`,`insert_date`,`featured`)
VALUES ('$title','$description','$location','$date','$link','$source','$insert_date','$featured');";

    return do_query($con, $insert_sql);
}

function update_item($id, $title, $description, $location, $date, $link, $source, $featured) {

    global $con;

    $title = mysqli_real_escape_string($con, $title);
    $description = mysqli_real_escape_string($con, $description);
    $location = mysqli_real_escape_string($con, $location);
    $link = mysqli_real_escape_string($con, $link);
    $date = mysqli_real_escape_string($con, $date);
    $source = mysqli_real_escape_string($con, $source);
    $featured = mysqli_real_escape_string($con, $featured);

    $sql = "UPDATE `jobs` SET 
               `title`='" . $title . "',
               `description`='" . $description . "',
               `location`='" . $location . "',
               `source`='" . $source . "',
               `date`='" . $date . "',
               `link`='" . $link . "', 
               `featured`='" . $featured . "' 
            WHERE `id`='" . $id . "'";

    return $result = do_query($con, $sql);
}

function item_exists($link) {

    global $con;

    $link = mysqli_real_escape_string($con, $link);

    $select_sql = "SELECT `id` FROM `jobs` WHERE link='" . $link . "'";
    $result = do_query($con, $select_sql);

    if ($result && mysqli_num_rows($result) > 0) {
        return TRUE;
    } else {
        return FALSE;
    }
}

function get_html($url) {

    $html = "";

    if ($curl = curl_init()) {
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Linux x86_64; rv:49.0) Gecko/20100101 Firefox/49.0');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 8);
        $curl_html = curl_exec($curl);
        $html = str_get_html($curl_html);
        unset($curl_html);
        curl_close($curl);
    }

    return $html;
}

function get_items() {

    global $con;

    $items = array();
    $sql = "SELECT * FROM `jobs` ORDER BY `insert_date` DESC";
    $result = do_query($con, $sql);

    while ($row = mysqli_fetch_assoc($result)) {
        $items[] = $row;
    }

    return $items;
}

function delete_items($ids_string) {

    global $con;

    $sql = "DELETE FROM `jobs` WHERE `id` IN (" . $ids_string . ")";
    return $result = do_query($con, $sql);
}

function ids_array_to_ids_string($ids_array) {
    $ids_string = "";
    foreach ($ids_array as $id) {
        if ($ids_string)
            $ids_string .= ",";
        $ids_string .= $id;
    }

    return $ids_string;
}

function add_rss_content($query) {

    global $rssfeed;
    global $con;

    $result = do_query($con, $query);
    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {

        $title = filter_value($row['title']);
        $company = filter_value($row['description']);
        $job_location = filter_value($row['location']);
        $link = filter_value($row['link']);
        $date = filter_value($row['insert_date']);

        if ($title) {
            $rssfeed .= "<item>";
            $rssfeed .= "<title>$title</title>";
            $rssfeed .= "<description>$company</description>";
            $rssfeed .= "<jobLocation>$job_location</jobLocation>";
            $rssfeed .= "<link>$link</link>";
            $rssfeed .= "<pubDate>$date</pubDate>";
            $rssfeed .= "</item>";
        }
    }
}

function add_content($query) {

    global $output;
    global $con;

    $result = do_query($con, $query);
    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {

        $title = filter_value($row['title']);
        $company = filter_value($row['description']);
        $job_location = filter_value($row['location']);
        $link = filter_value($row['link']);
        $date = filter_value($row['insert_date']);

        if ($title) {
            $output .= "<div class='job_item' >";
            $output .= "<h2><a href='" . $link . "' class='job_title'>$title</a><h2>";
            $output .= "<p class='job_company'>$company</p>";
            $output .= "<p class='job_location'>$job_location</p>";
            $output .= "<p class='job_date'>$date</p>";
            $output .= "</div>";
            $output .= "<div style='clear:both'></div>";
        }
    }
}

function rss_from_html($html) {

    global $rssfeed;
    global $con;

    $items = $html->find(".job_item");
    foreach ($items as $item) {

        $title = get_value($item, ".job_title", 0, "text");
        $company = get_value($item, ".job_company", 0, "text");
        $job_location = get_value($item, ".job_location", 0, "text");
        $link = get_value($item, ".job_title", 0, "href");
        $date = get_value($item, ".job_date", 0, "text");

        if ($title) {
            $rssfeed .= "<item>";
            $rssfeed .= "<title>$title</title>";
            $rssfeed .= "<description>$company</description>";
            $rssfeed .= "<jobLocation>$job_location</jobLocation>";
            $rssfeed .= "<link>$link</link>";
            $rssfeed .= "<pubDate>$date</pubDate>";
            $rssfeed .= "</item>";
        }
    }
}

function filter_value($value) {
    $value = str_replace("&", "&amp;", $value);
    return $value;
}
