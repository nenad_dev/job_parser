<?php
// start session   
session_start();

// security constant for including files
define("SECURE_ACCESS_INCLUDE_FILE", "ALLOW ACCESS");

// include script configuration
require_once "configuration.php";

// include script functions
require_once "functions.php";

// connect to db
$con = db_connect($db_host, $db_name, $db_user, $db_password);

// initialize login message
$wrong_login_message = "";

if (isset($_SESSION['is_logged_in']) && $_SESSION['is_logged_in'] == "yes") {
    header('Location: dashboard.php');
} elseif (isset($_POST["login"]) && ($_POST["submitted"] == "1")) {
    $username = filter_var($_POST["username"], FILTER_SANITIZE_STRING);
    $password = filter_var($_POST["password"], FILTER_SANITIZE_STRING);
    if (($username == $admin_username) && ($password == $admin_password)) {
        $_SESSION['is_logged_in'] = "yes";
        header('Location: dashboard.php');
    } else {
        $wrong_login_message = "wrong";
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <title>Login</title>
        <link rel="stylesheet" type="text/css" href="css/login.css" />
    </head>
    <body>
        <div class="container">
            <header>
                <h2>- Login page -</h2>
            </header>
            <section>
                <div id="container" >    
                    <div id="wrapper">
                        <div class="login">
                            <form id='login_form' action='index.php' method='post' accept-charset='UTF-8'>
                                <input type='hidden' name='submitted' id='submitted' value='1'/>
                                <div> 
                                    <label for='username' >Username:</label>
                                    <input type='text' name='username' id='username'  maxlength="50"  />
                                </div>
                                <div> 
                                    <label for='password' >Password:</label>
                                    <input type='password' name='password' id='password' maxlength="50"  />
                                </div>
                                <div>
                                    <input type='submit' name='login' value='Login' />
                                </div>
                                <?php if ($wrong_login_message == "wrong") echo "<p>Invalid username or password.</p> "; ?>
                            </form>
                        </div> 
                    </div>
                </div>    
            </section> 
        </div>        
    </body>
</html>

