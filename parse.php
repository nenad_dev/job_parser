<?php

// security constant for including files
define("SECURE_ACCESS_INCLUDE_FILE", "ALLOW ACCESS");

// include script configuration
require realpath(dirname(__FILE__)) . "/configuration.php";

// include script functions
require realpath(dirname(__FILE__)) . "/functions.php";

// include simple html dom parser
require realpath(dirname(__FILE__)) . "/simple_html_dom.php";

// connect to db
$con = db_connect($db_host, $db_name, $db_user, $db_password);

// get info
$url1 = "https://www.karir.com/sitemap.xml";
get_items_karir($url1);

$url2 = "https://www.blogger.com/feeds/975124612620016544/posts/default";
get_items_blogger($url2);

$url3 = "https://www.urbanhire.com/jobs";
get_items_urbanhire($url3);

$url4 = "https://www.karirpad.com/xml/jobFeed-Bosloker.xml";
get_items_karirpad($url4);

$url5 = "https://id.bosloker.com/featured-jobs/";
get_items_bosloker($url5, 1);

$url6 = "https://id.bosloker.com/semua-lowongan-kerja/";
get_items_bosloker($url6, 2);