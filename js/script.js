$(document).ready(function () {

    /* Init the table */
    oTable = $('#apps_table').DataTable({"order": []});

    $('body').delegate("#apps_table tbody tr", 'click', function () {
        if ($(this).hasClass("row_selected")) {
            $(this).removeClass('row_selected');
        } else {
            $(this).addClass('row_selected');
        }
    });

    /* Add a click handler for delete row */
    $('#delete_btn').click(function () {
        delete_row();
    });

    /* Add a click handler for add row */
    $('#new_btn').click(function () {
        add_row(validator);
    });

    /* Add a click handler for update row */
    $('#edit_btn').click(function () {
        update_row(validator, "update");
    });

    //-------- form validation --------------//
    var validator = $("#add_update_form").validate();
});


function add_row(validator) {
    var title, success_message, error_message, anSelected, tr;
    var info_array = [];
    var form_dialog;

    // reset form popup    
    reset_form_popup(validator);

    title = "Add New Row";
    success_message = "Row(s) succesfully added";
    error_message = "Some issue appeared during adding new row";
    $('#add_update_form')[0].reset();

    form_dialog = $("#add_update_popup").dialog({
        title: title,
        height: 'auto',
        width: 800,
        modal: true,
        buttons: {
            "Cancel": function () {
                $(this).dialog("close");
            },
            "Save": {
                text: "Save",
                id: "save_btn",
                click: function () {

                    if ($("#add_update_form").valid()) {
                        // create add array for sending info inserted in add form
                        add_item_in_array(
                                info_array,
                                $("#title").val(),
                                $("#description").val(),
                                $("#location").val(),
                                $("#source").val(),
                                $("#link").val(),
                                $("#featured").val()
                                );

                        // send info to server over POST 
                        post_send_info(info_array, "", success_message, error_message, "add", form_dialog);

                        // initialize info array
                        info_array = [];
                    }
                }
            }
        }
    });
}

function update_row(validator, action) {
    var title, success_message, error_message, id, anSelected, selected_tr;
    var info_array = [];
    var ids = [];
    var form_dialog;

    // reset form popup    
    reset_form_popup(validator);

    anSelected = fnGetSelected(oTable);

    if (anSelected.length != 1) {

        notification_popup("Please select one row", "Notification");

        $("#apps_table tbody tr").removeClass('row_selected');
        return;
    }

    // selected row id
    id = anSelected.attr("data-id");

    // selected row
    selected_tr = $('tr[data-id="' + id + '"]');

    // field values from selected row
    var title = selected_tr.find(".title").text();
    var desc = selected_tr.find(".description").text();
    var location = selected_tr.find(".location").text();
    var source = selected_tr.find(".source").text();
    var link = selected_tr.find(".link").text();
    var featured = selected_tr.find(".featured").text();

    // set values in update dialog with values from selected row
    $("#title").val(title);
    $("#description").val(desc);
    $("#location").val(location);
    $("#source").val(source);
    $("#link").val(link);
    $("#featured").val(featured);

    title = "Update Row";
    success_message = "Row succesfully updated";
    error_message = "Some issue appeared during updating row";

    form_dialog = $("#add_update_popup").dialog({
        title: title,
        height: 'auto',
        width: 800,
        modal: true,
        buttons: {
            "Cancel": function () {
                $(this).dialog("close");
            },
            "Save": {
                text: "Save",
                id: "save_btn",
                click: function () {
                    if ($("#add_update_form").valid()) {

                        add_item_in_array(
                                info_array,
                                $("#title").val(),
                                $("#description").val(),
                                $("#location").val(),
                                $("#source").val(),
                                $("#link").val(),
                                $("#featured").val()
                                );

                        // send info to server over POST        
                        post_send_info(info_array, id, success_message, error_message, action, form_dialog);

                        // initialize info array
                        info_array = [];
                    }
                }
            }
        }
    });
}

// delete row
function delete_row() {
    var anSelected = fnGetSelected(oTable);
    var info_array = [];
    var popup_title;

    var success_message = "Row(s) succesfully deleted";
    var error_message = "Some issue appeared during deleting process";
    if (anSelected.length > 1) {
        popup_title = "Are you sure that you want to delete these rows?";
    } else {
        popup_title = "Are you sure that you want to delete this row?";
    }

    anSelected.each(function () {
        info_array.push($(this).attr("data-id"));
    });

    if (info_array) {
        $('<div></div>').appendTo('body')
                .html('<div><h3>' + popup_title + '</h3></div>')
                .dialog({
                    modal: true,
                    title: 'Delete Confirmation',
                    zIndex: 10000,
                    autoOpen: true,
                    width: 'auto',
                    resizable: false,
                    buttons: {
                        Yes: function () {
                            $(this).dialog("close");

                            // send info to server over POST 
                            post_send_info(info_array, "", success_message, error_message, "delete", "");

                            // initialize info array
                            info_array = [];
                        },
                        No: function () {
                            $(this).dialog("close");
                        }
                    },
                    close: function (event, ui) {
                        $(this).remove();
                    }
                });
    } else {
        notification_popup("Please select row for deleting", "Notification");
    }
}

function add_item_in_array(info_array, title, description, location, source, link, featured) {
    var item_array = {
        title: title,
        description: description,
        location: location,
        source: source,
        link: link,
        featured: featured
    };

    return info_array.push(item_array);
}

function post_send_info(info_array, id, success_message, error_message, action, form_dialog) {
    var success_message_full;
    $("#loader").fadeIn();
    $.post('sql_management.php', {
        "info_array": info_array,
        "id": id,
        "action": action
    }, function (data) {
        var json = $.parseJSON(data);
        var status = json.status;
        var note = json.note;

        $("#loader").fadeOut("fast");

        if (status == "ok") {
            if (action != "delete") {
                form_dialog.dialog("close");
            }
            window.location.reload();
        } else {
            notification_popup(error_message, "Notification");
        }
    });
}

function reset_form_popup(validator) {
    validator.resetForm();
    $("#main_form").css("border", "none");
}

/* Get the rows which are currently selected */
function fnGetSelected(oTableLocal)
{
    return oTableLocal.$('tr.row_selected');
}

// custom dialog popup
function notification_popup(content, title) {
    $("<div><h3>" + content + "</h3></div>").dialog({
        title: title,
        width: 400,
        buttons: {
            "OK": function () {
                $(this).dialog("close");
            }
        }
    });
}