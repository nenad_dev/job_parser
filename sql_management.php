<?php

// security constant for including files
define("SECURE_ACCESS_INCLUDE_FILE", "ALLOW ACCESS");

// include script configuration
require realpath(dirname(__FILE__)) . "/configuration.php";

// include script functions
require realpath(dirname(__FILE__)) . "/functions.php";

// connect to db
$con = db_connect($db_host, $db_name, $db_user, $db_password);

// initialize result from executing sql query
$result = "";
$note = "";

// if info is submitied from main page
if (isset($_POST["action"])) {

// add/update info
    if (($_POST["action"] == "add") || ($_POST["action"] == "update")) {

        // get add array that contain form data
        $info_array = $_POST["info_array"];
        $info_item = $info_array[0];

        $title = filter_var($info_item["title"], FILTER_SANITIZE_STRING);
        $description = filter_var($info_item["description"], FILTER_SANITIZE_STRING);
        $location = filter_var($info_item["location"], FILTER_SANITIZE_STRING);
        $source = filter_var($info_item["source"], FILTER_SANITIZE_STRING);
        $link = filter_var($info_item["link"], FILTER_SANITIZE_STRING);
        $featured = filter_var($info_item["featured"], FILTER_SANITIZE_NUMBER_INT);

        if ($_POST["action"] == "add") {
            $result = insert_in_db($title, $description, $location, "", $link, $source, $featured);
        } elseif ($_POST["action"] == "update") {
            $id = filter_var($_POST["id"], FILTER_SANITIZE_NUMBER_INT);
            $result = update_item($id, $title, $description, $location, "", $link, $source, $featured);
        }
    } elseif ($_POST["action"] == "delete") {
        $ids_string = ids_array_to_ids_string($_POST["info_array"]);
        if ($ids_string) {
            $result = delete_items($ids_string);
        }
    }
}

if ($result) {
    echo json_encode(array(
        "status" => "ok"
    ));
} else {
    echo json_encode(array(
        "status" => "error"
    ));
}

